window.onload = () => {

	// 保存したurlとusernameを上部に常時表示するようにする
	$('.url-wrapper').append(`${localStorage.url ?
	 	`<p class="show-info">${localStorage.url}</p>` : 
	 	`<p class="warning">Please save url.</p>`}`);
	$('.username-wrapper').append(`${localStorage.username ?
	 	`<p class="show-info">${localStorage.username}</p>` : 
	 	`<p class="warning">Please save username.</p>`}`);



	// nameとurlをlocalstrageに保存しておく
	$("#url-submit").on('click', () => {
		$("#set-url").submit((e) => {
			const url = $("#set-url [name=url]").val();
			localStorage.setItem('url', url);
			console.log(url)
			e.preventDefault();
			reload();
		})
	})
	$("#username-submit").on('click', () => {
		$("#set-username").submit((e) => {
			const username = $("#set-username [name=username]").val();
			localStorage.setItem('username', username);
			e.preventDefault();
			reload();
		})
	})


	// messageのPOST後にリロードして画面リセット
	const reload = () => {
		setTimeout(() => {
			location.reload();
		}, 500);
	};


	// iconType識別用
	let iconType;
	const checkIcon = (e) => {
		// 2個目のクラス名取得
		const target = e.target.className.split(' ')[1];
		switch(target) {
			case 'cry':
				iconType = 0;
				break;
			case 'exciting':
				iconType = 1;
				break;
			case 'loving':
				iconType = 2;
				break;
			case 'reply':
				iconType = 3;
				break;
			default:
				break;
		}
		return iconType;
	}


	// messageの値をPOSTする
	const ajaxPost = (message, e) => {
		const data = {
			text: message,
			username: localStorage.username
		}
		const jsonData = JSON.stringify(data);
		e.preventDefault();
		$.ajax({
			url: localStorage.url,
			dataType: 'text',
			data: 'payload=' + jsonData,
			type: 'POST',
			processData: false,
		}).done((data) => {
			console.log('POST suceeded');
		}).fail((xhr, status, errorThrown) => {
			console.log(errorThrown.toString());
		});
		reload();
	}


	// 各messageアイコンをホバー時にmessageを表示する
	$('.icon').hover((e) => {
		checkIcon(e);
		$('.message-detail-wrapper').append(`<p class="icon-message">${message_data[iconType].message}</p>`);
	}, () => {
		$('.icon-message').remove();
	});


	// 各messageアイコンをクリックでmessageをPOST
	$('.icon').on('click', (e) => {
		checkIcon(e);
		const message = message_data[iconType].message;
		ajaxPost(message, e);
	})



	// 入力messageを送る
	$("#post-submit").on('click', () => {
		$("#post-form").submit((e) => {
			const message = $("#post-form [name=message]").val();
			ajaxPost(message, e);
		});
	})

}